/*
Package swag converts Go annotations to Swagger Documentation 2.0.
See https://gitlab.com/shadowy-go/swag for more information about swag.
*/
package swag // import "gitlab.com/shadowy-go/swag"
